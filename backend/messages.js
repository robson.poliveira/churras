
const MSGS = {
    'CATEGORY404' : 'Categoria não encontrada',
    'GENERIC_ERROR': 'Erro!',
    'PRODUCT404' : 'Produto não encontrada',
    'USER404' : 'Usuário não encontrado',
}

module.exports = MSGS