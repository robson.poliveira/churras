import styled from 'styled-components'
import React from 'react'
import Menu from './menu'
import Products from './products'

const Home = () => {
    return (
        <HomeContainer>
            <Menu />
            <Products />
        </HomeContainer>
    )
}

const HomeContainer = styled.div`
    background: #ccc;
`
// const IconHome = styled(BiHome)`
// color: red
// `

export default Home